require 'webrick'
require 'chess'

#g = Chess::Game.new
#
#until g.over? && 2==3
#  begin
#    print "Give me a #{g.active_player} move: "
#    input = gets.chop
#    break if input == 'quit'
#    g << input
#    puts g
#    puts g.moves.last
#  rescue Chess::IllegalMoveError => e
#    puts 'Illegal move!'
#  end
#end
#puts g.status

class Aj
  def initialize
    @partidas = Hash.new
  end

  def msg( str )
    if str == "nueva" 
      #orl: nueva|otra
      #-> crea nueva partida p1, muestra tablero
      @partidas["p1"] = Chess::Game.new;
      #return @partidas["p1"].to_s;
      return @partidas["p1"].current.to_fen;
    end
    
    if str == "ver p1"
      #xxx: ver p1
      #--> muestra tablero
      #return @partidas["p1"].to_s;    
      return @partidas["p1"].current.to_fen;
    end
    
    if str[0,2] == "p1"
      puts str
      move=str[2..-1].strip
      puts move
      #orl: p1 e4
      #--> actualiza partida p1(nueva jugada), muestra tablero
      @partidas[ "p1" ] << move
      #return @partidas["p1"].to_s
      return @partidas["p1"].current.to_fen;
    end
    
    if str == "partidas"
      #xxx: partidas
      #--> muestra lista de partidas con sus jugadores
      return @partidas
    end
  
    return "hola dijiste #{str}"
  end  #fin metodo msg
end #fin clase Aj

aj = Aj.new

server = WEBrick::HTTPServer.new :Port => 8080

server.mount_proc '/' do |req, res|

  if req.query["msg"]
    straux = aj.msg( req.query["msg"] );
    straux = "<img src=https://www.janko.at/Retros/d.php?ff=" + straux + ">"

    msgo = "<div class=msg>" + straux + "</div>"
  else
    msgo = ""
  end
  form = '<form action="/"><label>msg</label> <input type=text name=msg>'
   '<input type=submit></form>'
  res.body = '<html>' + form + msgo + '</html>'
end

trap 'INT' do server.shutdown end

server.start


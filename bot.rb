require 'net/http'
require 'json'
require 'date'
require "pstore"

# to-do
# que el mozo no interactue con telegram_... sino con un objeto chat que recibe en sehabla
# enviar mail a samara
# poder forzar por mensaje el enviarorden
# variar respuestas a los pedidos ( ahora solo dice muy buena elec... )
# sobreescribir comensal si pide dos veces el mismo

class Mozo

	def initialize
		store = PStore.new( "store.data")
		store.transaction {
			@pedidos = store[ :pedidos ]
			if @pedidos.nil? then
				@pedidos = Pedidos.new
			end
		}
		puts @pedidos
	end
	
	def sehabla( msg, debug = 1 )
		#puts "#{self.class.name}.#{__method__.to_s} #{Time.now}"

		puts "soy el mozo, #{msg.quien} me dijo el #{msg.cuando} lo siguiente: #{msg.que} en chat #{msg.chatid}"

		if msg.que =~ /opci.{1,4}n/i && msg.que !~ /les paso el men/i then
			if msg.que.match( /(.*)(orlando|alejandro|jeremias|jere|claudio|samara|francisco|fran|santiago|santi|victor)(.*)/i ) then
				comensal = $2
				plato = $1 + " " + $3
			else
				puts "mozo: comensal no identificado en mensaje, se asume que es el que habla, #{msg.quien}"
				comensal = msg.quien
				plato = msg.que
			end
			if debug then
				puts "gracias #{comensal}, muy buena eleccion"
			else
				telegram_enviar( "gracias #{comensal}, muy buena eleccion", msg.chatid )
			end
			@pedidos << Pedido.new( Date.parse( msg.cuando.to_s ), comensal.strip.downcase, plato.strip, msg.chatid )
		else
			puts "mozo: no se habla de un pedido"
		end #fin se habla de un pedido
	end #fin se-habla

	def pedidospara( fec )
		#puts "#{self.class.name}.#{__method__.to_s} #{Time.now}"
		@pedidos.select { |ped| ped.dia.strftime("%m/%d/%Y") == fec.strftime("%m/%d/%Y") }
	end

	def telegram_enviar( txt, chatid = "693889331" )
		url = URI.escape( "https://api.telegram.org/bot679134165:AAGqH014Q0lKocwpzuMRsUjuXSu_JVvQSD8/sendMessage?chat_id=" + chatid.to_s + "&text=#{txt}" )
		uri = URI.parse( url )
		puts "#{self.class.name}.#{__method__.to_s} #{Time.now}" + ": #{uri}"
		http = Net::HTTP.new( uri.host, uri.port )
		http.use_ssl = true
		request = Net::HTTP::Get.new( uri.request_uri )

		response = http.request(request)
		json = JSON.parse response.body
	end
end #fin mozo

class Mensaje
  attr_reader :quien, :que, :cuando, :chatid
	def initialize( quien, que, cuando, chatid )
		@quien = quien
		@que = que
		@cuando = cuando
		@chatid = chatid
	end

end


class Pedido
  attr_reader :dia, :quien, :plato, :chatid, :servido

	def initialize( dia, quien, plato, chatid, servido = false )
		@dia = dia
		@quien = quien
		@plato = plato
		@chatid = chatid
		@servido = servido
	end

end

class Bot
	def initialize( agenda )
		@agenda = agenda
		@mozo = Mozo.new
		@tlg_lastupd = -1
	end

	def trabajar( debug = 1 )
		#puts "#{self.class.name}.#{__method__.to_s} #{Time.now}"

		while true

			t = @agenda.quehay

			if debug then
				msgs = self.stdin_mensajes
				msgs.each { | unmsg | 
					self.sehabla( unmsg )
				}
			
			elsif t then
				puts "bot: agenda indica #{t.quehacer} entre #{t.desde} y #{t.hasta} [#{Time.now}]"

				if( t.quehacer == "tomarpedidos" ) then

					msgs = self.telegram_mensajes;	
					
					msgs.each { | unmsg | 
						self.sehabla( unmsg )
					}
					t.listo

				elsif( t.quehacer == "enviarordenmenu" ) then
					enviarordenmenu
					t.listo
				else
					puts "bot: ERROR actividad #{t.quehacer} desconocida"
					exit
				end
			else #fin hay actividad
				#puts "bot: sin actividad [#{Time.now}]"
			end
		sleep(5)
		end #fin w-hile
	end

	def sehabla( mensaje )
		#puts "#{self.class.name}.#{__method__.to_s} #{Time.now}"
		if mensaje.que =~ /listo mozo/i then
			enviarordenmenu
		else
			@mozo.sehabla( mensaje )
		end
	end

	def enviarordenmenu( debug = 1 )
		pedidosenchat = Hash.new
		#puts "#{self.class.name}.#{__method__.to_s} #{Time.now}"
		pedidos = @mozo.pedidospara( Time.now )
		puts "bot: enviando pedido, hoy es #{Date.today()}"
		pedidos.each { | unped | 
			if pedidosenchat[unped.chatid].nil? then
				pedidosenchat[unped.chatid] = "#{unped.quien}: #{unped.plato}\n"
			else
				pedidosenchat[unped.chatid] += "#{unped.quien}: #{unped.plato}\n"
			end
		}
		pedidosenchat.each { |chatid,pedidogral| 
			if ! pedidogral.empty? then
				#telegram_enviar( pedidotexto, '-278754715' )
				if debug then
					puts pedidogral
				else
					telegram_enviar( pedidogral, chatid )
				end
				@pedidos.servidos( chatid, Time.now )
				
			end
		}

	end

	def stdin_mensajes
		mensajes = []
		que = gets
		quien = "stdin"
		cuando = Time.now
		chatid = "stdin"
		unmsg = Mensaje.new( quien, que, cuando, chatid )
		mensajes << unmsg
		mensajes
		
	end #fin std-in-mensajes
	
	def telegram_mensajes

		#json = '{"ok":true,"result":[{"update_id":161402725, "message":{"message_id":9,"from":{"id":693889331,"is_bot":false,"first_name":"orlando","last_name":"biazzi","language_code":"es"},"chat":{"id":693889331,"first_name":"orlando","last_name":"biazzi","type":"private"},"date":1537659585,"text":"Alejandro opci\u00f3n 1 con fruta"}}]}'
		#json = JSON.parse json

		url = "https://api.telegram.org/bot679134165:AAGqH014Q0lKocwpzuMRsUjuXSu_JVvQSD8/getUpdates?offset=#{ @tlg_lastupd+1 }"
		uri = URI.parse( url )
		#puts "#{self.class.name}.#{__method__.to_s} #{Time.now}" + ": #{uri}"
		http = Net::HTTP.new( uri.host, uri.port )
		http.use_ssl = true
		request = Net::HTTP::Get.new( uri.request_uri )

		#trend da error con cntlm para telegram, si se soluciona entonces descomentar las siguientes dos lineas y comentar el w-get
		response = http.request(request)
		json = JSON.parse response.body
	
		#value = `wget --quiet --no-check-certificate -e http_proxy="https://127.0.0.1:3128" -e https_proxy="http://127.0.0.1:3128"  -O- #{url}`
		#json = JSON.parse value;

		#{"ok":true,"result":[]}

		# {
		# 	"ok":true,
		# 	"result":[
		# 		{
		# 			"update_id":161402722,
		# 			"message":{
		# 				"message_id":6,
		# 				"from":{
		# 					"id":693889331,
		# 					"is_bot":false,
		# 					"first_name":"orlando",
		# 					"last_name":"biazzi",
		# 					"language_code":"es"
		# 				},
		# 				"chat":{
		# 					"id":693889331,
		# 					"first_name":"orlando",
		# 					"last_name":"biazzi",
		# 					"type":"private"
		# 				},
		# 				"date":1537655846,
		# 				"text":"ok"
		# 			} //fin m-essage
		# 		}
		# 	]
		# }

		results = json['result']

		mensajes = []

		results.each { | result |
			que = result['message']['text']
			quien = result['message']['from']['first_name']
			cuando = Time.at( result['message']['date'] )
			chatid = result['message']["chat"]["id"]
			unmsg = Mensaje.new( quien, que, cuando, chatid )
			mensajes << unmsg
			@tlg_lastupd = result['update_id']
		}
		mensajes
	end #fin t-elegram-mensajes

	def telegram_enviar( txt, chatid = "693889331" )
		#txt.gsub!( " ", "%20")
		url = URI.escape( "https://api.telegram.org/bot679134165:AAGqH014Q0lKocwpzuMRsUjuXSu_JVvQSD8/sendMessage?chat_id=" + chatid.to_s + "&text=#{txt}" )
		uri = URI.parse( url )
		puts "#{self.class.name}.#{__method__.to_s} #{Time.now}" + ": #{uri}"
		http = Net::HTTP.new( uri.host, uri.port )
		http.use_ssl = true
		request = Net::HTTP::Get.new( uri.request_uri )

		#trend da error con cntlm para telegram, si se soluciona entonces descomentar las siguientes dos lineas y comentar el w-get
		response = http.request(request)
		json = JSON.parse response.body
		#value = `wget --no-check-certificate -e http_proxy="https://127.0.0.1:3128" -e https_proxy="http://127.0.0.1:3128"  -O- "#{url}"`
		#json = value;

	end
end


class Tarea
	attr_reader :quehacer, :desde, :hasta, :lista, :hacerunasolavez
	def initialize( que, desde = Time.now, hasta = Time.now + 60*5, unasolavez = true )
		@quehacer = que
		@desde = desde
		@hasta = hasta
		@lista = false
		@hacerunasolavez = unasolavez
	end

	def listo
		@lista = true
	end

end

class Agenda
	def initialize( tareas = [] )
		@tareas = tareas
	end

	def quehay
		aresponder = nil
		@tareas.each { |tarea| 
			if ( ! tarea.hacerunasolavez || ! tarea.lista ) && Time.now >= tarea.desde && Time.now <= tarea.hasta then
				aresponder = tarea
				break
			end
		}
		aresponder
	end
end

class Pedidos < Array
	def <<( obj )
		super
		puts "que"
		store = PStore.new( "store.data" );
		store.transaction {
			store[ :pedidos ] = self
		}
	end
	def servidos( chatidx, fecx ) 
		self.each { |ped|
			if ped.chatid == chatidx && ped.dia.strftime("%m/%d/%Y") == fecx.strftime("%m/%d/%Y") && ped.servido == false then
				ped.servido = true
			end
		}
		store = PStore.new( "store.data" );
		store.transaction {
			store[ :pedidos ] = self
		}
	end
end

agenda = Agenda.new( [ 
Tarea.new( 'tomarpedidos', Time.new( 2019, 1, 28,16, 20, 0 ), Time.new( 2019,1, 28, 17, 45, 0 ), false ), Tarea.new( 'enviarordenmenu', Time.new( 2019,1,28,16, 15, 1 ), Time.new( 2019,1, 28, 16, 20, 0 ), true ),
Tarea.new( 'tomarpedidos', Time.new( 2019, 1, 29,10, 0, 0 ), Time.new( 2019,1, 29, 10, 2, 0 ), false ), Tarea.new( 'enviarordenmenu', Time.new( 2019,1,29,10, 2, 1 ), Time.new( 2019,1, 29, 10,3, 0 ), true ),
Tarea.new( 'tomarpedidos', Time.new( 2019, 1, 30,8, 0, 0 ), Time.new( 2019,1, 30, 9, 25, 0 ), false ), Tarea.new( 'enviarordenmenu', Time.new( 2019,1,30,9, 30, 0 ), Time.new( 2019,1, 30, 9,35, 0 ), true ),
Tarea.new( 'tomarpedidos', Time.new( 2019, 1, 31,8, 0, 0 ), Time.new( 2019,1, 31, 9, 25, 0 ), false ), Tarea.new( 'enviarordenmenu', Time.new( 2019,1,31,9, 30, 0 ), Time.new( 2019,1, 31, 9,35, 0 ), true ),
Tarea.new( 'tomarpedidos', Time.new( 2019, 2, 1,8, 0, 0 ), Time.new( 2019,2, 1, 9, 25, 0 ), false ), Tarea.new( 'enviarordenmenu', Time.new( 2019,2,1,9, 30, 0 ), Time.new( 2019,2, 1, 9,35, 0 ), true ),
Tarea.new( 'tomarpedidos', Time.new( 2019, 2, 2,8, 0, 0 ), Time.new( 2019,2, 2, 9, 25, 0 ), false ), Tarea.new( 'enviarordenmenu', Time.new( 2019,2,2,9, 30, 0 ), Time.new( 2019,2, 2, 9,35, 0 ), true ),
Tarea.new( 'tomarpedidos', Time.new( 2019, 2, 3,8, 0, 0 ), Time.new( 2019,2, 3, 9, 25, 0 ), false ), Tarea.new( 'enviarordenmenu', Time.new( 2019,2,3,9, 30, 0 ), Time.new( 2019,2, 3, 9,35, 0 ), true ),
Tarea.new( 'tomarpedidos', Time.new( 2019, 2, 4,8, 0, 0 ), Time.new( 2019,2, 4, 9, 25, 0 ), false ), Tarea.new( 'enviarordenmenu', Time.new( 2019,2,4,9, 30, 0 ), Time.new( 2019,2, 4, 9,35, 0 ), true ),
Tarea.new( 'tomarpedidos', Time.new( 2019, 2, 5,8, 0, 0 ), Time.new( 2019,2, 5, 9, 25, 0 ), false ), Tarea.new( 'enviarordenmenu', Time.new( 2019,2,5,9, 30, 0 ), Time.new( 2019,2, 5, 9,35, 0 ), true ),
Tarea.new( 'tomarpedidos', Time.new( 2019, 2, 6,8, 0, 0 ), Time.new( 2019,2, 6, 9, 25, 0 ), false ), Tarea.new( 'enviarordenmenu', Time.new( 2019,2,6,9, 30, 0 ), Time.new( 2019,2, 6, 9,35, 0 ), true ),
Tarea.new( 'tomarpedidos', Time.new( 2019, 2, 7,8, 0, 0 ), Time.new( 2019,2, 7, 9, 25, 0 ), false ), Tarea.new( 'enviarordenmenu', Time.new( 2019,2,7,9, 30, 0 ), Time.new( 2019,2, 7, 9,35, 0 ), true ),
Tarea.new( 'tomarpedidos', Time.new( 2019, 2, 8,8, 0, 0 ), Time.new( 2019,2, 8, 9, 25, 0 ), false ), Tarea.new( 'enviarordenmenu', Time.new( 2019,2,8,9, 30, 0 ), Time.new( 2019,2, 8, 9,35, 0 ), true ),
Tarea.new( 'tomarpedidos', Time.new( 2019, 2, 9,8, 0, 0 ), Time.new( 2019,2, 9, 9, 25, 0 ), false ), Tarea.new( 'enviarordenmenu', Time.new( 2019,2,9,9, 30, 0 ), Time.new( 2019,2, 9, 9,35, 0 ), true ),
Tarea.new( 'tomarpedidos', Time.new( 2019, 2, 10,8, 0, 0 ), Time.new( 2019,2, 10, 9, 25, 0 ), false ), Tarea.new( 'enviarordenmenu', Time.new( 2019,2,10,9, 30, 0 ), Time.new( 2019,2, 10, 9,35, 0 ), true ),
Tarea.new( 'tomarpedidos', Time.new( 2019, 2, 11,8, 0, 0 ), Time.new( 2019,2, 11, 9, 25, 0 ), false ), Tarea.new( 'enviarordenmenu', Time.new( 2019,2,11,9, 30, 0 ), Time.new( 2019,2, 11, 9,35, 0 ), true ),
Tarea.new( 'tomarpedidos', Time.new( 2019, 2, 12,8, 0, 0 ), Time.new( 2019,2, 12, 9, 25, 0 ), false ), Tarea.new( 'enviarordenmenu', Time.new( 2019,2,12,9, 30, 0 ), Time.new( 2019,2, 12, 9,35, 0 ), true ),
Tarea.new( 'tomarpedidos', Time.new( 2019, 2, 13,8, 0, 0 ), Time.new( 2019,2, 13, 9, 25, 0 ), false ), Tarea.new( 'enviarordenmenu', Time.new( 2019,2,13,9, 30, 0 ), Time.new( 2019,2, 13, 9,35, 0 ), true ),
Tarea.new( 'tomarpedidos', Time.new( 2019, 2, 14,8, 0, 0 ), Time.new( 2019,2, 14, 9, 25, 0 ), false ), Tarea.new( 'enviarordenmenu', Time.new( 2019,2,14,9, 30, 0 ), Time.new( 2019,2, 14, 9,35, 0 ), true ),
Tarea.new( 'tomarpedidos', Time.new( 2019, 2, 15,8, 0, 0 ), Time.new( 2019,2, 15, 9, 25, 0 ), false ), Tarea.new( 'enviarordenmenu', Time.new( 2019,2,15,9, 30, 0 ), Time.new( 2019,2, 15, 9,35, 0 ), true ),
Tarea.new( 'tomarpedidos', Time.new( 2019, 2, 16,8, 0, 0 ), Time.new( 2019,2, 16, 9, 25, 0 ), false ), Tarea.new( 'enviarordenmenu', Time.new( 2019,2,16,9, 30, 0 ), Time.new( 2019,2, 16, 9,35, 0 ), true ),
Tarea.new( 'tomarpedidos', Time.new( 2019, 2, 17,8, 0, 0 ), Time.new( 2019,2, 17, 9, 25, 0 ), false ), Tarea.new( 'enviarordenmenu', Time.new( 2019,2,17,9, 30, 0 ), Time.new( 2019,2, 17, 9,35, 0 ), true ),
Tarea.new( 'tomarpedidos', Time.new( 2019, 2, 25,9, 30, 0 ), Time.new( 2019,2, 25, 9, 25, 0 ), false ), Tarea.new( 'enviarordenmenu', Time.new( 2019,2,25,9, 30, 0 ), Time.new( 2019,2, 25, 9,35, 0 ), true )
 ] )
bot = Bot.new( agenda )

bot.trabajar

